import org.junit.Assert;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TasksTest {
    Tasks t = new Tasks();

    @Test
    public void testSentenceWithVolew() {
        String string1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";

        var result = t.Task2(string1);
        Assert.assertEquals(result.split(" ")[1], "consectetur");
    }

    @Test
    public void testSentenceWithoutVolew() {
        String string2 = "Lorem  dolor sit, consectetur. ";

        var result = t.Task2(string2);
        Assert.assertEquals(result, string2);
    }
}