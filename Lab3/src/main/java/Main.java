import java.io.*;
import java.util.*;

public class Main
{
    public static void main(String[] args) throws IOException
    {
        Tasks tasks = new Tasks();
        ArrayList<String> words = new ArrayList<String>();
        String text = "";

        File file1 = new File("src/main/file1.txt");

        Scanner scnr = new Scanner(file1);

        do
        {
            String data1 = scnr.nextLine();
            if(data1 != null)
            {
                text += data1;
                StringTokenizer strtok = new StringTokenizer(data1, " ");

                while(strtok.hasMoreTokens())
                {
                    words.add(strtok.nextToken());
                }

            }
        }while(scnr.hasNextLine());


        while(true)
        {
            Scanner sc = new Scanner(System.in);
            System.out.println("\nEnter the number of task to perform: ");
            int input = sc.nextInt();
            switch(input)
            {
                case 1:
                    tasks.Task1(text, words);
                    break;
                case 2:
                    System.out.println("Enter your text");
                    Scanner scanner = new Scanner( System. in);

                    String in = scanner.nextLine();
                    tasks.Task2(in);
                    break;

                default:
                    break;
            }
        }
    }
}
