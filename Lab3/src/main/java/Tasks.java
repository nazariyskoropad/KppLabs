
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

public class Tasks {
    public void Task1(String text, ArrayList<String> words)
    {
        ArrayList<String> newWords = new ArrayList<String>();
        String[] formats = {
                "yyyy-MM-dd",
                "yyyy:MM:dd",
                "dd/MM/yyyy",
                "yyyyMMdd"
        };

        for(var word : words)
        {
            boolean isDate = false;
            for (String parse : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(parse);
                try {
                    var date = sdf.parse(word); //get current date
                    Date tomorrow = new Date(date.getTime() + (1000 * 60 * 60 * 24)); //get tomorrow date
                    String newDate = sdf.format(tomorrow); //transform date to string
                    var simpleDateformat = new SimpleDateFormat("EEEE"); //to get day name

                    var newWord = newDate + "(" + simpleDateformat.format(tomorrow) + ")";
                    newWords.add(newWord);
                    isDate = true;
                    break;
                } catch (ParseException e) {
                }
            }
            if(!isDate)
                newWords.add(word);
        }

        var newText = "";
        for(String item : newWords)
        {
            newText += item + " ";
        }

        System.out.print(newText);
    }

    public String Task2(String text)
    {
        String[] sentences = text.split("(?<=(?<![A-Z])\\.)");
        Pattern p = Pattern.compile("^[aieouAIEOU].*");

        var newWords = new ArrayList<String>();

        for(var sentence : sentences)
        {
            String[] words = sentence.split(" ");
            String longest = Arrays.stream(sentence.split(" ")).max(Comparator.comparingInt(String::length)).orElse(null);
            boolean found = false;
            for(var word : words)
            {
                if(!found && p.matcher(word).find())
                {
                    newWords.add(longest);
                    found = true;
                    continue;
                }
                newWords.add(word);
            }
        }

        var newText = "";
        for(String item : newWords)
        {
            newText += item + " ";
        }

        System.out.print(newText);
        return newText;
    }

}



//Варіант 10.
// Знайти всі дати, задані різними поширеними форматами. Визначити, в якому діапазоні вони знаходяться.
// Поміняти всі  дати на наступний день, вставивши після них в дужках день тижня, на який ця дата припадає.

//   У кожному реченні тексту поміняти місцями перше слово, що починається на голосну букву
//   з найдовшим словом. Текст  слід ввести з консолі
