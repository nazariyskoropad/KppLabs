public class Goods implements Comparable<Goods>
{
    protected int price;
    protected TargetGender targetGender;
    protected Season season;


    public Goods(int price, TargetGender targetGender, Season season)
    {
        this.price = price;
        this.targetGender = targetGender;
        this.season = season;
    }


    public int getPrice()
    {
        return price;
    }

    public TargetGender getTargetGender()
    {
        return targetGender;
    }

    public Season getSeason()
    {
        return season;
    }

    @Override
    public int compareTo(Goods g) {
        return this.price - g.price;
    }

    @Override
    public String toString() {
        return "Goods";
    }
}
