public class Bag extends Goods
{
    protected int sectionCount;

    public Bag(int price, TargetGender targetGender, Season season, int sectionCount)
    {
        super(price, targetGender, season);
        this.sectionCount = sectionCount;
    }


    public int sectionCount()
    {
        return sectionCount;
    }

    @Override
    public String toString() {
        return "Bag";
    }
}
