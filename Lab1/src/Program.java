import java.util.ArrayList;

public class Program {
    public static void main(String[] args) {
        var goods = new ArrayList<Goods>();
        goods.add(new Bag(220, TargetGender.Boy, Season.Fall, 3));
        goods.add(new Clothing(100, TargetGender.Girl, Season.Fall, ClothingType.Pants, 12));
        goods.add(new Accessory(45, TargetGender.Girl, Season.NotSeasonal, Accessory.AccessoryType.Glasses));
        goods.add(new Bag(235, TargetGender.Girl, Season.Fall, 3));
        goods.add(new Clothing(90, TargetGender.Boy, Season.Fall, ClothingType.Shirt, 12));
        goods.add(new Accessory(55, TargetGender.Unisex, Season.NotSeasonal, Accessory.AccessoryType.Belt));

        GoodsManager.Sort.byPrice(goods, GoodsManager.Order.Ascending);


        System.out.println("Sorter 1:");

        System.out.println("Goods storage, sorted by price in ascending order:");
        GoodsManager.print(goods);
        GoodsManager.Sort.byPrice(goods, GoodsManager.Order.Descending);
        System.out.println("\nGoods storage, sorted by price in descending order:");
        GoodsManager.print(goods);

        System.out.println("\nSorter 2:");
        var manager = new GoodsManager();
        manager.sortByPrice(goods, GoodsManager.Order.Ascending);
        System.out.println("Goods storage, sorted by price:");
        GoodsManager.print(goods);

        var clothingStorage = new ArrayList<Clothing>();
        clothingStorage.add(new Clothing(90, TargetGender.Boy, Season.Fall, ClothingType.Shirt, 22));
        clothingStorage.add(new Clothing(90, TargetGender.Boy, Season.Fall, ClothingType.Shirt, 11));
        clothingStorage.add(new Clothing(90, TargetGender.Boy, Season.Fall, ClothingType.Shirt, 15));
        clothingStorage.add(new Clothing(90, TargetGender.Boy, Season.Fall, ClothingType.Shirt, 15));
        clothingStorage.add(new Clothing(90, TargetGender.Boy, Season.Fall, ClothingType.Shirt, 14));
        clothingStorage.add(new Clothing(90, TargetGender.Boy, Season.Fall, ClothingType.Shirt, 4));
        clothingStorage.add(new Clothing(90, TargetGender.Boy, Season.Fall, ClothingType.Shirt, 6));

        System.out.println("\nSorter 3:");
        GoodsManager.LambdaSorter.sort(clothingStorage, (x, y)-> x.getSize() - y.getSize());
        System.out.println("Clothes storage, sorted by size with lambda:");
        GoodsManager.printClothing(clothingStorage);

        var foundGoods = GoodsManager.find(goods, (g) -> {
            boolean seasonIsCorrect = g.getSeason() == Season.Fall || g.getSeason() == Season.NotSeasonal;
            boolean genderIsCorrect = g.getTargetGender() == TargetGender.Girl || g.getTargetGender() == TargetGender.Unisex;
            return seasonIsCorrect && genderIsCorrect;
        });

        System.out.println("\nFinder:");
        System.out.println("Goods for girls in fall season:");
        GoodsManager.print(foundGoods);
    }
}
