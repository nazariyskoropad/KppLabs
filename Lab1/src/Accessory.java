public class Accessory extends Goods
{
    public enum AccessoryType
    {
        Glasses,
        Tie,
        Belt
    }

    AccessoryType accessoryType;

    public Accessory(int price, TargetGender targetGender, Season season, AccessoryType accessoryType)
    {
        super(price,  targetGender, season);
        this.accessoryType = accessoryType;
    }

    @Override
    public String toString() {
        return "Accessory";
    }

}
