public class Clothing extends Goods
{
    protected ClothingType clothingType;
    protected int size;

    public Clothing(int price, TargetGender targetGender, Season season, ClothingType clothingType, int size)
    {
        super(price, targetGender, season);
        this.clothingType = clothingType;
        this.size = size;
    }


    public ClothingType getClothingType()
    {
        return clothingType;
    }

    public int getSize()
    {
        return size;
    }

    @Override
    public String toString() {
        return "Clothing";
    }
}
