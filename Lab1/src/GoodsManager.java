import java.util.ArrayList;
import java.util.Collections;
import java.util.function.BiFunction;
import java.util.function.Function;

public class GoodsManager
{
    enum Order
    {
        Ascending,
        Descending
    }

    public static class Sort
    {
        public static void byPrice(ArrayList<Goods> goods, Order order)
        {
            if(order == Order.Ascending)
            {
                Collections.sort(goods);
            }
            else
            {
                Collections.sort(goods, Collections.reverseOrder());
                System.out.println();
            }
        }
    }

    public class Sorter
    {
        public void SortByPrice(ArrayList<Goods> goods, Order order)
        {
            if(order == Order.Ascending)
            {
                Collections.sort(goods);
            }
            else
            {
                Collections.sort(goods, Collections.reverseOrder());
            }
        }
    }

    void sortByPrice(ArrayList<Goods> goods, Order order)
    {
        Sorter sorter = new Sorter();
        sorter.SortByPrice(goods, order);
    }

    public static class LambdaSorter
    {
        public static void sort(ArrayList<Clothing> goods, BiFunction<Clothing, Clothing, Integer> function)
        {
            Collections.sort(goods, function::apply);
        }
    }

    static void print(ArrayList<Goods> goods)
    {
        for(Goods g : goods)
        {
            System.out.printf("Goods type: %s, price: %d, season: %s, target gender: %s\n",
                    g.toString(), g.getPrice(), g.getSeason().toString(), g.getTargetGender().toString());
        }
    }

    static void printClothing(ArrayList<Clothing> goods)
    {
        for(Clothing g : goods)
        {
            System.out.printf("Clothing: price: %d, season: %s, target gender: %s, size: %d\n",
                    g.getPrice(), g.getSeason().toString(), g.getTargetGender().toString(), g.getSize());
        }
    }

    static ArrayList<Goods> find(ArrayList<Goods> storage, Function<Goods, Boolean> condition)
    {
        var res = new ArrayList<Goods>();
        for(Goods g : storage)
        {
            if(condition.apply(g))
            {
                res.add(g);
            }
        }
        return res;
    }
}
