package com.company;

import java.util.ArrayList;

public class Lecturer
{
    private String Name;
    private String Discipline;
    private ArrayList<String> Students;

    Lecturer() {}
    Lecturer(String Name, String Discipline, ArrayList<String> Students)
    {
        this.Name = Name;
        this.Discipline = Discipline;
        this.Students = Students;
    }
    public String GetName() { return this.Name; }
    public String GetDiscipline() { return this.Discipline; }
    public ArrayList<String> GetStudents() { return this.Students; }

    public void SetName(String Name) { this.Name = Name; }
    public void SetDiscipline(String Discipline) { this.Discipline = Discipline; }
    public void SetStudents(ArrayList<String> Students) { this.Students = Students; }

    @Override
    public int hashCode()
    {
        return (this.GetName().hashCode()) +
                (this.GetDiscipline().hashCode()) +
                (this.GetStudents().hashCode());
    }
    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(!(obj instanceof Lecturer))
            return false;
        if(obj == this)
            return true;
        return (this.GetName().equals(((Lecturer) obj).GetName()))
                && (this.GetDiscipline().equals(((Lecturer) obj).GetDiscipline()))
                && (this.GetStudents().equals(((Lecturer) obj).GetStudents()));
    }

}
