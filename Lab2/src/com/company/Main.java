package com.company;
import java.io.*;
import java.util.*;

public class Main
{
    public static void main(String[] args) throws IOException
    {
        Manager Manager = new Manager();
        ArrayList<Lecturer> lecturers1 = new ArrayList<Lecturer>();

        FileReader freader1 = new FileReader("file1.txt");
        BufferedReader buffreader1 = new BufferedReader(freader1);
        String data1;
        do
        {
            data1 = buffreader1.readLine();
            if(data1 != null)
            {
                StringTokenizer strtok = new StringTokenizer(data1, ",");
                String name = strtok.nextToken();
                String discipline = strtok.nextToken();
                ArrayList<String> students = new ArrayList<String>();

                //String token = strtok.nextToken();
                while(strtok.hasMoreTokens())
                {
                    students.add(strtok.nextToken());
                //    token = strtok.nextToken();
                }

                lecturers1.add(new Lecturer(name, discipline, students));
            }
        }while(data1 != null);
        buffreader1.close();

        while(true)
        {
            Scanner sc = new Scanner(System.in);
            // menu
            System.out.println("\nEnter the number of task to perform: ");
            int input = sc.nextInt();
            switch(input)
            {
                case 1:
                    Manager.Task1(lecturers1);
                    break;
                case 2:
                    System.out.println("All disciplines");
                    for(var lecturer : lecturers1)
                        System.out.print(lecturer.GetDiscipline()+ " ");
                    System.out.println("\nEnter first discipline");
                    String dis1 = sc.next();
                    System.out.println("Enter second discipline");
                    String dis2 = sc.next();

                    Manager.Task2(lecturers1, dis1, dis2);
                    break;
                    case 3:
                        ArrayList<Lecturer> lecturers2 = new ArrayList<Lecturer>();

                        FileReader freader2 = new FileReader("file2.txt");
                        BufferedReader buffreader2 = new BufferedReader(freader2);
                        String data2;
                        do
                        {
                            data2 = buffreader2.readLine();
                            if(data2 != null)
                            {
                                StringTokenizer strtok = new StringTokenizer(data2, ",");
                                String name = strtok.nextToken();
                                String discipline = strtok.nextToken();
                                ArrayList<String> students = new ArrayList<String>();

                                while(strtok.hasMoreTokens())
                                {
                                    students.add(strtok.nextToken());
                                }

                                lecturers2.add(new Lecturer(name, discipline, students));
                            }
                        }while(data2 != null);
                    Manager.Task3(lecturers1, lecturers2);
                    break;
                default:
                    break;
            }
        }
    }
}
