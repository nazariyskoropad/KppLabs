package com.company;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Manager
{
   public void Task1(ArrayList<Lecturer> lecturers)
   {
       HashMap<String, ArrayList<String>> hm = new HashMap<String, ArrayList<String>>();

       ArrayList<String> studs = new ArrayList<>();

       for(var lecturer : lecturers)
       {
           for (var student : lecturer.GetStudents()) {
               hm.put(student, null);
           }
       }

       for(var key : hm.keySet())
       {
           ArrayList<String> al = new ArrayList<String>();

           for(var lecturer : lecturers)
           {
               if(lecturer.GetStudents().contains(key))
               {
                   al.add(lecturer.GetDiscipline());
               }
           }

           hm.put(key, al);
       }
       System.out.println("Task #1: ----------------------->");
       PrintMap(hm);
   }

    public void Task2(ArrayList<Lecturer> lecturers, String dis1, String dis2)
    {
        HashMap<String, HashSet<String>> hm = new HashMap<>();
        for(var lecturer: lecturers)
        {
            hm.put(lecturer.GetDiscipline(), new HashSet<>(lecturer.GetStudents()));
        }

        HashSet<String> set = new HashSet<>();
        for (var key : hm.keySet())
        {
            set = hm.get(key);
            for(var key1 : hm.keySet())
            {
                set.retainAll(hm.get(key1));
            }
            break;
        }

        System.out.println("Students that visit all lectures" + set);

        var set1 = hm.get(dis1);
        var set2 = hm.get(dis2);
        set1.retainAll(set2);
        System.out.println("\nStudents that visit both " + dis1 + " " + dis2 + " " + set1);
    }

    public void Task3(ArrayList<Lecturer> lecturers1, ArrayList<Lecturer> lecturers2)
    {
        lecturers1.addAll(lecturers2);
        HashMap<String, Integer> hm = new HashMap<String, Integer>();

        for(var lecturer : lecturers1)
        {
            hm.put(lecturer.GetName(), 0);
        }
        for(var key : hm.keySet())
        {
            int count = 0;
            for(var lecturer : lecturers1)
            {
                var str = lecturer.GetName();
                if(str.equals(key))
                    count++;
            }
            hm.put(key, count);
        }

        var avg = (float)lecturers1.stream().count()/hm.size();
        System.out.println("Average number of disciplines for each lecturer = " + avg);

        HashMap<String, Integer> hm1 = new HashMap<String, Integer>();

        for(var key : hm.keySet())
        {
            if(hm.get(key) > avg)
                hm1.put(key, hm.get(key));
        }

        System.out.println("Task3");
        for(var key : hm1.keySet())
        {
            System.out.println(key + " " + hm1.get(key));
        }
    }

    public void PrintMap(HashMap<String, ArrayList<String>> hm)
    {
        for(var key : hm.keySet())
        {
            System.out.println("\nStudent: " + key + ".");
            for( var val : hm.get(key))
                System.out.print(val + " ");
        }
    }
}/*
    Задано інформацію про викладачів, яка включає прізвище, назву курсу та список студентів, що зараховані
        слухачами на заданий курс.
        1.	Створити таблицю, в першій колонці якої є прізвище студента, а в другій – перелік дисциплін, які він слухає.
        2.	Створити список студентів, які зараховані на всі дисципліни. Визначити спільних слухачів 2 заданих курсів
        3.	З 2 різних файлів зчитати 2 вихідні набори інформації про викладачів. Створити спільну колекцію, яка складатиметься з усіх викладачів, які мають кількість курсів вищу, від середньої по узагальненій колекції, без повторів.
*/